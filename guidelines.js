// npm install express express-fileupload mysql2 ejs
// npm install --save-dev nodemon morgan

// agregar "start": "nodemon src/index.js" en scripts

// crear folder src

// index.js


import express from "express";
import path from "path";
import morgan from "morgan";
import fileUpload from 'express-fileupload';
import { fileURLToPath } from "url";

import excursionRoutes from "./routes/excursiones.routes.js";

// crear folder routes
// /routes/excursiones.routes.js

import { Router } from "express";
import {
  createExcursiones,
  deleteExcursiones,
  editExcursiones,
  renderExcursiones,
  updateExcursiones,
} from "../controllers/excursionesController.js";
const router = Router();

router.get("/", renderExcursiones);
router.post("/add", createExcursiones);
router.get("/update/:id", editExcursiones);
router.post("/update/:id", updateExcursiones);
router.get("/delete/:id", deleteExcursiones);

export default router;

// crear folder controllers
// /controllers/excursionesController.js

import { pool } from "../db.js";
import fs from 'fs';

// crear /src db.js

import { createPool } from "mysql2/promise";

export const pool = createPool({
  host: "localhost",
  user: "root",
  password: "",
  port: 3306,
  database: "db_excursiones",
});

// volvemos a index.js

const app = express();
const __dirname = path.dirname(fileURLToPath(import.meta.url));

// carga de archivos
app.use(fileUpload());

// configuración
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));

// routes
app.use(excursionRoutes);

// static files
app.use(express.static(path.join(__dirname, "public")));


const port = process.env.PORT || 3000;
app.listen(port);

console.log(`Servidor iniciado en el puerto ${port}`);


// volvemos a /controllers/excursionesController.js

export const renderExcursiones = async (req, res) => {
    const [rows] = await pool.query("SELECT * FROM excursiones");
    res.render("excursiones", { excursiones: rows });
  };
  
  export const createExcursiones = async (req, res) => {
    if (!req.files || Object.keys(req.files).length === 0 || !req.files.image) {
      return res.status(400).send("No se ha seleccionado ninguna imagen.");
    }
  
    const image = req.files.image;
  
    const uploadPath = "./uploads/";
  
    if (!fs.existsSync(uploadPath)) {
      fs.mkdirSync(uploadPath);
      console.log("Directorio creado.");
    }
  
    const fileName = image.name;
    const finalPath = uploadPath + fileName;
  
    image.mv(finalPath, (err) => {
      if (err) {
        return res.status(500).send("Error al guardar la imagen.");
      }
    });
  
    const newExcursion = req.body;
  
    newExcursion.imagen = finalPath;
    await pool.query("INSERT INTO excursiones set ?", [newExcursion]);
    res.redirect("/");
  };
  
  export const editExcursiones = async (req, res) => {
    const { id } = req.params;
    const [result] = await pool.query("SELECT * FROM excursiones WHERE id = ?", [
      id,
    ]);
    res.render("excursiones_edit", { excursion: result[0] });
  };
  
  export const updateExcursiones = async (req, res) => {
    const { id } = req.params;
    const newExcursion = req.body;

    if (!req.files || Object.keys(req.files).length === 0 || !req.files.image) {
        console.log("Imagen preservada");

    }else{
        const image = req.files.image;
        const uploadPath = "./uploads/";
        if (!fs.existsSync(uploadPath)) {
          fs.mkdirSync(uploadPath);
          console.log("Directorio creado.");
        }
      
        const fileName = image.name;
        const finalPath = uploadPath + fileName;
        image.mv(finalPath, (err) => {
          if (err) {
            return res.status(500).send("Error al guardar la imagen.");
          }
        });
      
        const newExcursion = req.body;
        newExcursion.imagen = finalPath;
    }

    delete newExcursion.image;
    await pool.query("UPDATE excursiones set ? WHERE id = ?", [newExcursion, id]);
    res.redirect("/");
  };
  
  export const deleteExcursiones = async (req, res) => {
    const { id } = req.params;
    const result = await pool.query("DELETE FROM excursiones WHERE id = ?", [id]);
    if (result.affectedRows === 1) {
      res.json({ message: "Excursión Eliminada!" });
    }
    res.redirect("/");
  };
  

  // crear folder views
  // crear excursiones.ejs

  
  <%- include("includes/_header") %>

  <div class="container mt-5">
    <div class="row">
      <div class="col-md-5">
        <div class="card-body">
          <form action="/add" method="POST" enctype="multipart/form-data">
            <div class="form-group">
              <textarea
                name="descripcion"
                placeholder="Descripción"
                class="form-control mb-2 textarea"
                autofocus
              ></textarea>
            </div>
  
            <div class="form-group">
              <input
                type="date"
                name="fecha"
                placeholder="Fecha"
                class="form-control mb-2"
              />
            </div>
            <div class="form-group">
              <input
                type="text"
                name="precio"
                placeholder="Precio"
                class="form-control mb-2"
              />
            </div>
            <div class="form-group">
              <input
                type="text"
                name="estado"
                placeholder="Estado"
                class="form-control mb-2"
              />
            </div>
            <div class="form-group">
              <textarea
                name="lugar"
                placeholder="Lugar"
                class="form-control mb-2 textarea"
              ></textarea>
            </div>
            <div class="form-group">
              <input
                type="text"
                name="capacidad"
                placeholder="Capacidad"
                class="form-control mb-2"
              />
            </div>
            <div class="form-group">
              <input
                type="hidden"
                name="imagen"
                placeholder="Image"
                class="form-control mb-2"
              />
            </div>
  
            <div class="form-group">
              <input type="file" name="image" accept="image/*" />
            </div>
  
            <button type="submit" class="btn btn-info">Guardar</button>
          </form>
        </div>
      </div>
    </div>
  
    <div class="row">
      <div class="col-md-7">
        <table class="table table-dark table-bordered table-hover">
          <thead>
            <tr>
              <td>n°</td>
              <td>Descripción</td>
              <td>Fecha</td>
              <td>Precio</td>
              <td>Estado</td>
              <td>Lugar</td>
              <td>Capacidad</td>
              <td>Imagen</td>
              <td class="text-center">Acciones</td>
            </tr>
          </thead>
          <tbody>
            <% if (excursiones) { %> <% for(var i = 0; i < excursiones.length;
            i++) { %>
            <tr>
              <td><%= (i + 1) %></td>
              <td><%= excursiones[i].descripcion %></td>
              <td><%= excursiones[i].fecha %></td>
              <td><%= excursiones[i].precio %></td>
              <td><%= excursiones[i].estado %></td>
              <td><%= excursiones[i].lugar %></td>
              <td><%= excursiones[i].capacidad %></td>
              <td><%= excursiones[i].imagen %></td>
              <td class="d-flex gap-2">
                <a href="/update/<%= excursiones[i].id %>" class="btn btn-info">
                  Editar
                </a>
                <a href="/delete/<%= excursiones[i].id %>" class="btn btn-danger">
                  Eliminar
                </a>
              </td>
            </tr>
            <% } %> <% } %>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  
  <%- include("includes/_footer") %>
  

// crear folder includes
// crear _footer / _header
</body>
</html>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Excursiones</title>
    <!-- <link rel="stylesheet" href="https://bootswatch.com/5/lux/bootstrap.min.css"> -->
    <link rel="stylesheet" href="/styles/index.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <a class="navbar-brand" href="/">Excursiones</a>
      </div>
    </nav>
  </body>
</html>


// crear excursiones_edit.ejs

<%- include("includes/_header") %>

<div class="container mt-5">
  <div class="row">
    <div class="col-md-5 offset-md-3 card bg-dark">
      <div class="card-body">
        <form
          action="/update/<%= excursion.id %>"
          method="POST"
          enctype="multipart/form-data"
        >
          <div class="form-group">
            <textarea
              name="descripcion"
              placeholder="Descripción"
              class="form-control mb-2 textarea"
              autofocus
            >
<%= excursion.descripcion %></textarea
            >
          </div>
          <div class="form-group">
            <input
              type="date"
              name="fecha"
              value="<%= excursion.fecha %>"
              placeholder="Fecha"
              class="form-control mb-2"
            />
          </div>
          <div class="form-group">
            <input
              type="text"
              name="precio"
              value="<%= excursion.precio %>"
              placeholder="Precio"
              class="form-control mb-2"
            />
          </div>
          <div class="form-group">
            <input
              type="text"
              name="estado"
              value="<%= excursion.estado %>"
              placeholder="Estado"
              class="form-control mb-2"
            />
          </div>
          <div class="form-group">
            <textarea
              name="lugar"
              placeholder="Lugar"
              class="form-control mb-2 textarea"
            >
<%= excursion.lugar %></textarea
            >
          </div>
          <div class="form-group">
            <input
              type="text"
              name="capacidad"
              value="<%= excursion.capacidad %>"
              placeholder="Capacidad"
              class="form-control mb-2"
            />
          </div>
          <div class="form-group">
            <input
              type="hidden"
              name="imagen"
              value="<%= excursion.imagen %>"
              placeholder="Imagen"
              class="form-control mb-2"
            />
          </div>

          <div class="form-group">
            <input type="file" name="image" accept="image/*" />
          </div>

          <button type="submit" class="btn btn-info">Guardar</button>
        </form>
      </div>
    </div>
  </div>
</div>

<%- include("includes/_footer") %>


// crear folder public/styles/index.css

/* Estilos generales */
body {
  font-family: Arial, sans-serif;
  margin: 0;
  padding: 0;
  background: #202020;
  color: white;
}

.container {
  max-width: 1200px;
  margin: 0 auto;
  padding: 0 20px;
}

/* Estilos de la barra de navegación */
.navbar {
  background-color: #343a40;
}

.navbar-brand {
  font-size: 1.5rem;
  font-weight: bold;
  color: #fff;
}

.navbar-brand:hover {
  color: #aaa;
}

.form-control {
  width: 500px;
}

.form-group {
  margin-bottom: 10px;
}

.card-body {
  margin-top: 20px;
}

.textarea {
  max-width: 750px;
  height: 120px;
  max-height: 200px;
}

.btn {
  padding: 10px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

.btn-info {
  background-color: #17a2b8;
  color: #fff;
}

.btn-info:hover {
  background-color: #138496;
}

.btn-danger {
  background-color: #dc3545;
  color: #fff;
}

.btn-danger:hover {
  background-color: #c82333;
}

.table {
  width: 100%;
  margin-bottom: 1rem;
  color: #fff;
  background-color: #343a40;
  border-collapse: collapse;
}

.table td,
.table th {
  padding: 0.75rem;
  vertical-align: top;
  border-top: 1px solid #dee2e6;
}

.table thead th {
  vertical-align: bottom;
  border-bottom: 2px solid #dee2e6;
}

.table-dark {
  color: inherit;
}

.table-dark td,
.table-dark th,
.table-dark thead th {
  border-color: #dee2e6;
}

.table-dark.table-bordered {
  border: 0;
}

.table-dark.table-bordered td,
.table-dark.table-bordered th {
  border: 1px solid #dee2e6;
}

.table-dark.table-bordered thead td,
.table-dark.table-bordered thead th {
  border-bottom-width: 2px;
}

.table-hover tbody tr:hover {
  background-color: rgba(255, 255, 255, 0.1);
}

.text-center {
  text-align: center;
}

.d-flex {
  display: flex;
}

.gap-2 {
  gap: 2px;
}
