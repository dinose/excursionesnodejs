import { pool } from "../db.js";
import fs from 'fs';

export const renderExcursiones = async (req, res) => {
    const [rows] = await pool.query("SELECT * FROM excursiones");
    res.render("excursiones", { excursiones: rows });
  };
  
  export const createExcursiones = async (req, res) => {
    if (!req.files || Object.keys(req.files).length === 0 || !req.files.image) {
      return res.status(400).send("No se ha seleccionado ninguna imagen.");
    }
  
    const image = req.files.image;
  
    const uploadPath = "./uploads/";
  
    if (!fs.existsSync(uploadPath)) {
      fs.mkdirSync(uploadPath);
      console.log("Directorio creado.");
    }
  
    const fileName = image.name;
    const finalPath = uploadPath + fileName;
  
    image.mv(finalPath, (err) => {
      if (err) {
        return res.status(500).send("Error al guardar la imagen.");
      }
    });
  
    const newExcursion = req.body;
  
    newExcursion.imagen = finalPath;
    await pool.query("INSERT INTO excursiones set ?", [newExcursion]);
    res.redirect("/");
  };
  
  export const editExcursiones = async (req, res) => {
    const { id } = req.params;
    const [result] = await pool.query("SELECT * FROM excursiones WHERE id = ?", [
      id,
    ]);
    res.render("excursiones_edit", { excursion: result[0] });
  };
  
  export const updateExcursiones = async (req, res) => {
    const { id } = req.params;
    const newExcursion = req.body;

    if (!req.files || Object.keys(req.files).length === 0 || !req.files.image) {
        console.log("Imagen preservada");

    }else{
        const image = req.files.image;
        const uploadPath = "./uploads/";
        if (!fs.existsSync(uploadPath)) {
          fs.mkdirSync(uploadPath);
          console.log("Directorio creado.");
        }
      
        const fileName = image.name;
        const finalPath = uploadPath + fileName;
        image.mv(finalPath, (err) => {
          if (err) {
            return res.status(500).send("Error al guardar la imagen.");
          }
        });
      
        const newExcursion = req.body;
        newExcursion.imagen = finalPath;
    }

    delete newExcursion.image;
    await pool.query("UPDATE excursiones set ? WHERE id = ?", [newExcursion, id]);
    res.redirect("/");
  };
  
  export const deleteExcursiones = async (req, res) => {
    const { id } = req.params;
    const result = await pool.query("DELETE FROM excursiones WHERE id = ?", [id]);
    if (result.affectedRows === 1) {
      res.json({ message: "Excursión Eliminada!" });
    }
    res.redirect("/");
  };
  