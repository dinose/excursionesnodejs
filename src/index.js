import express from "express";
import path from "path";
import morgan from "morgan";
import fileUpload from 'express-fileupload';
import { fileURLToPath } from "url";

import excursionRoutes from "./routes/excursiones.routes.js";

const app = express();
const __dirname = path.dirname(fileURLToPath(import.meta.url));

// carga de archivos
app.use(fileUpload());

// archivos estáticos
app.use(express.static(path.join(__dirname, "public")));

// configuración
app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));

// routes
app.use(excursionRoutes);


const port = process.env.PORT || 3000;
app.listen(port);

console.log(`Servidor iniciado en el puerto ${port}`);